//CREATE DATABASE AND USE IT, APPEND HASH TO DATABASE NAME
CREATE DATABASE e01526534ADBSDB_Wetyzthcq23lypuio;
USE e01526534ADBSDB_Wetyzthcq23lypuio;

//Create the standard, non-indexed and non-partitioned tables and load the data
//datafales were previously put into hdfs under their respective names
// we will read timestamps as strings and convert them later accordingly due to mismatch between the file format and the hive timestamp format (using T instead of whitespace)
CREATE TABLE e01526534ADBSDB_Wetyzthcq23lypuio.BADGES ( id BIGINT,class BIGINT,date STRING,name string,tagbased boolean,userid BIGINT ) ROW FORMAT DELIMITED FIELDS TERMINATED BY ',';
LOAD DATA INPATH 'badges' INTO TABLE e01526534ADBSDB_Wetyzthcq23lypuio.BADGES;

CREATE TABLE e01526534ADBSDB_Wetyzthcq23lypuio.POSTS ( id BIGINT,acceptedanswerid BIGINT,answercount BIGINT,body STRING,closeddate STRING,commentcount BIGINT,communityowneddate STRING,creationdate STRING,favoritecount BIGINT,
lastactivitydate STRING,lasteditdate STRING,lasteditordisplayname STRING,lasteditoruserid BIGINT,ownerdisplayname STRING,owneruserid BIGINT,parentid BIGINT,posttypeid BIGINT,score BIGINT,tags STRING,title STRING,viewcount BIGINT
) ROW FORMAT DELIMITED FIELDS TERMINATED BY ',';
LOAD DATA INPATH 'posts' INTO TABLE e01526534ADBSDB_Wetyzthcq23lypuio.POSTS;

CREATE TABLE e01526534ADBSDB_Wetyzthcq23lypuio.POSTLINKS ( id BIGINT ,creationdate STRING,linktypeid BIGINT,postid BIGINT,relatedpostid BIGINT) ROW FORMAT DELIMITED FIELDS TERMINATED BY ',';
LOAD DATA INPATH 'postlinks' INTO TABLE e01526534ADBSDB_Wetyzthcq23lypuio.POSTLINKS;

CREATE TABLE e01526534ADBSDB_Wetyzthcq23lypuio.USERS ( id BIGINT,aboutme STRING,accountid BIGINT,creationdate STRING,displayname STRING,downvotes BIGINT,lastaccessdate STRING,location STRING,profileimageurl STRING,reputation BIGINT,
upvotes BIGINT,views BIGINT,websiteurl STRING) ROW FORMAT DELIMITED FIELDS TERMINATED BY ',';
LOAD DATA INPATH 'users' INTO TABLE e01526534ADBSDB_Wetyzthcq23lypuio.USERS;

CREATE TABLE e01526534ADBSDB_Wetyzthcq23lypuio.VOTES ( id BIGINT,bountyamount BIGINT,creationdate STRING,postid BIGINT,userid BIGINT,votetypeid BIGINT) ROW FORMAT DELIMITED FIELDS TERMINATED BY ',';
LOAD DATA INPATH 'votes' INTO TABLE e01526534ADBSDB_Wetyzthcq23lypuio.VOTES;

CREATE TABLE e01526534ADBSDB_Wetyzthcq23lypuio.COMMENTS (id BIGINT,creationdate STRING,postid BIGINT,score BIGINT,text STRING,userdisplayname STRING,userid BIGINT) ROW FORMAT DELIMITED FIELDS TERMINATED BY ',';
LOAD DATA INPATH 'comments' INTO TABLE e01526534ADBSDB_Wetyzthcq23lypuio.COMMENTS;


//execute the query once on unpartitioned and unbucketet data
EXPLAIN EXTENDED SELECT p.id FROM posts p, comments c, users u, votes v WHERE c.postid=p.id AND c.userid=p.owneruserid AND u.id=p.owneruserid AND u.reputation > 100 AND v.postid = p.id AND v.userid = p.owneruserid AND NOT EXISTS (SELECT 1 FROM postlinks l WHERE l.relatedpostid = p.id);
EXPLAIN SELECT p.id FROM posts p, comments c, users u, votes v WHERE c.postid=p.id AND c.userid=p.owneruserid AND u.id=p.owneruserid AND u.reputation > 100 AND v.postid = p.id AND v.userid = p.owneruserid AND NOT EXISTS (SELECT 1 FROM postlinks l WHERE l.relatedpostid = p.id);



//set partitioning parameters
SET hive.enforce.bucketing = TRUE;
SET hive.exec.dynamic.partition = TRUE;
SET hive.exec.dynamic.partition.mode=nonstrict;
SET hive.exec.max.dynamic.partitions= 10000;
SET hive.exec.max.dynamic.partitions.pernode = 10000;
//potential partitioning bugfix
SET hive.optimize.sort.dynamic.partition=false;
SET hive.auto.convert.join=false;
SET mapred.compress.map.output=true;
SET hive.exec.parallel=true;
SET mapred.reduce.tasks=50;
//Create the partitioned, bucketed tables

/*
 THE COUNTS FOR DIFFERENT ATTRIBUTES ARE:
 posts.id 244156
 posts.owneruserid 78561
 votes.userid 21704
 votes.postid 274081
 users.id 235983
 users.reputation 1519
 postlinks.relatedpostid 12416
 comments.postid 117818
 comments.userid 40417

 Based on these we consider it best to hash on userid and partition on users.reputation. We would have preferred an attribute with significantly less distinct values, but given that most are ids this is unfortunately not realistically feasible.

 note: HIVE does not support < or > in partitioning, we would have liked to partition users by their reputation splitting at 100
 Partitioning will break the table row wise into subfolders containing only rows with the respective partitioning attribute value.
 Bucketing by userid will make it simpler for the query to find the relevant users (those with more than 100 reputation) in all the tables.
 Also we will bucket the postid since it is regularly used as join attribute.
 */

//we partition users by reputation and therefore do not include reputation in the users table beforehand

CREATE TABLE e01526534ADBSDB_Wetyzthcq23lypuio.BADGES_PB ( id BIGINT,class BIGINT,date STRING,name string,tagbased boolean,userid BIGINT ) CLUSTERED BY (id) SORTED BY (id ASC) INTO 100 BUCKETS ROW FORMAT DELIMITED FIELDS TERMINATED BY ',';
INSERT INTO e01526534ADBSDB_Wetyzthcq23lypuio.BADGES_PB select * from e01526534ADBSDB_Wetyzthcq23lypuio.BADGES;

CREATE TABLE e01526534ADBSDB_Wetyzthcq23lypuio.POSTS_PB ( id BIGINT,acceptedanswerid BIGINT,answercount BIGINT,body STRING,closeddate STRING,commentcount BIGINT,communityowneddate STRING,creationdate STRING,favoritecount BIGINT,
                                                       lastactivitydate STRING,lasteditdate STRING,lasteditordisplayname STRING,lasteditoruserid BIGINT,ownerdisplayname STRING,owneruserid BIGINT,parentid BIGINT,posttypeid BIGINT,score BIGINT,tags STRING,title STRING,viewcount BIGINT
) CLUSTERED BY (id) SORTED BY (id ASC) INTO 10 BUCKETS ROW FORMAT DELIMITED FIELDS TERMINATED BY ',';
INSERT INTO e01526534ADBSDB_Wetyzthcq23lypuio.POSTS_PB select * from e01526534ADBSDB_Wetyzthcq23lypuio.POSTS;

CREATE TABLE e01526534ADBSDB_Wetyzthcq23lypuio.POSTLINKS_PB ( id BIGINT ,creationdate STRING,linktypeid BIGINT,postid BIGINT,relatedpostid BIGINT) CLUSTERED BY (id) SORTED BY (id ASC) INTO 100 BUCKETS ROW FORMAT DELIMITED FIELDS TERMINATED BY ',';
INSERT INTO e01526534ADBSDB_Wetyzthcq23lypuio.POSTLINKS_PB select * from e01526534ADBSDB_Wetyzthcq23lypuio.POSTLINKS;

CREATE TABLE e01526534ADBSDB_Wetyzthcq23lypuio.USERS_PB ( id BIGINT,aboutme STRING,accountid BIGINT,creationdate STRING,displayname STRING,downvotes BIGINT,lastaccessdate STRING,location STRING,profileimageurl STRING,
                                                       upvotes BIGINT,views BIGINT,websiteurl STRING) PARTITIONED BY (reputation BIGINT)  CLUSTERED BY (id) SORTED BY (id ASC) INTO 100 BUCKETS ROW FORMAT DELIMITED FIELDS TERMINATED BY ',';
INSERT OVERWRITE TABLE e01526534ADBSDB_Wetyzthcq23lypuio.USERS_PB PARTITION(reputation) select * from e01526534ADBSDB_Wetyzthcq23lypuio.USERS;

CREATE TABLE e01526534ADBSDB_Wetyzthcq23lypuio.VOTES_PB ( id BIGINT,bountyamount BIGINT,creationdate STRING,postid BIGINT,userid BIGINT,votetypeid BIGINT)  CLUSTERED BY (id) SORTED BY (id ASC) INTO 100 BUCKETS ROW FORMAT DELIMITED FIELDS TERMINATED BY ',';
INSERT INTO e01526534ADBSDB_Wetyzthcq23lypuio.VOTES_PB select * from e01526534ADBSDB_Wetyzthcq23lypuio.VOTES;

CREATE TABLE e01526534ADBSDB_Wetyzthcq23lypuio.COMMENTS_PB (id BIGINT,creationdate STRING,postid BIGINT,score BIGINT,text STRING,userdisplayname STRING,userid BIGINT) CLUSTERED BY (id) SORTED BY (id ASC) INTO 100 BUCKETS ROW FORMAT DELIMITED FIELDS TERMINATED BY ',';
INSERT INTO e01526534ADBSDB_Wetyzthcq23lypuio.COMMENTS_PB select * from e01526534ADBSDB_Wetyzthcq23lypuio.COMMENTS;



//execute the query once on partitioned and bucketed data
EXPLAIN EXTENDED SELECT p.id FROM posts_pb p, comments_pb c, users_pb u, votes_pb v WHERE c.postid=p.id AND c.userid=p.owneruserid AND u.id=p.owneruserid AND u.reputation > 100 AND v.postid = p.id AND v.userid = p.owneruserid AND NOT EXISTS (SELECT 1 FROM postlinks l WHERE l.relatedpostid = p.id);
EXPLAIN SELECT p.id FROM posts_pb p, comments_pb c, users_pb u, votes_pb v WHERE c.postid=p.id AND c.userid=p.owneruserid AND u.id=p.owneruserid AND u.reputation > 100 AND v.postid = p.id AND v.userid = p.owneruserid AND NOT EXISTS (SELECT 1 FROM postlinks l WHERE l.relatedpostid = p.id);
 SELECT p.id FROM posts_pb p, comments_pb c, users_pb u, votes_pb v WHERE c.postid=p.id AND c.userid=p.owneruserid AND u.id=p.owneruserid AND u.reputation > 100 AND v.postid = p.id AND v.userid = p.owneruserid AND NOT EXISTS (SELECT 1 FROM postlinks l WHERE l.relatedpostid = p.id);


//CLEANUP: DROP THE ENTIRE DATABASE
DROP DATABASE e01526534ADBSDB_Wetyzthcq23lypuio CASCADE;

/* CORRELATED QUERY
With relevantposts AS (SELECT * FROM posts p where exists (SELECT 1 FROM postlinks l WHERE l.relatedpostid > p.id)),
usersbadge AS (SELECT * FROM ((SELECT (b.userid) from badges b WHERE (b.name SIMILAR TO ’Autobiographer’ OR b.name SIMILAR TO ’Supporter’)) as badge join users u on u.id = badge.userid)),
     upvotesbyusers AS (SELECT AVG(upvotes) as up FROM users)
SELECT rp.*,ub.*,c.*
from relevantposts rp join usersbadge ub on ub.id=rp.owneruserid
    ADBS (SS 2019) 1. Exercise – Jovan Jeromela (1528091); David Gradinariu (1526534) 14
join comments c on c.postid=rp.id, upvotesbyusers
where
(ub.creationdate > c.creationdate and
ub.upvotes >= upvotesbyusers.up -3)
*/

/*
 To adapt the query for hive we mainly did two different things:
 1) We replaced the average calculation with a noncorrelated counterpart.
 2) We select autobiographers beforehand.
 3) We use MAX for the related post ids. Idea behind it is that if the maximum value is not larger than the post id, then no other value in postlinks will be larger. This saves us from having to nest queries and gives us an efficiency bonus since postlinks are in no other way constrained
 by further where clauses (primitive max is therefore employable).
 */

WITH user_average AS (SELECT AVG(upvotes) AS avg_up FROM users),
     autobiographers AS (SELECT * FROM badges b WHERE b.name LIKE 'Autobiographer'),
     max_related_post_id AS (SELECT MAX(relatedpostid) AS max_id FROM postlinks)
SELECT DISTINCT p.id
FROM posts p, comments c, users u, autobiographers b, user_average, max_related_post_id
WHERE c.postid=p.id AND u.id = p.owneruserid  AND (from_unixtime(to_unix_timestamp (u.creationdate,"dd-MM-yyyy'T'HH:mm:ss")) > from_unixtime(to_unix_timestamp (c.creationdate,"dd-MM-yyyy'T'HH:mm:ss")) and u.upvotes >= user_average.avg_up -3) AND u.id = b.userid AND (max_related_post_id.max_id > p.id);

//DID NOT WORK: mapper stopped at 0%, probable cause: the cross product generated was overly large and therefore exceeded memory limits
//relevant_posts AS (SELECT DISTINCT p.id, p.owneruserid FROM posts p, postlinks pl WHERE pl.relatedpostid > p.id)