import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Cluster;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;


import java.io.IOException;
import java.util.*;

/**
 * A simple class that just counts the documents in every category. A combiner is used to bundle mapper outputs.
 * The overall logic is similar to WordCount:
 * Every mapper outputs <Category, 1>, with Category being the name of the respective category. The combiner then combines
 * the mapper records and the reducer sums up the category counts before writing them to file.
 */

public class Task1MapReduce
{

    public static class CatCounterMapper extends Mapper<Object, Text, Text, LongWritable>
    {


        HashMap<String,Long> categoryFrequencyMap;
        public void map(Object key, Text value, Context context
        ) throws IOException, InterruptedException {

        //todo: mapper logic
        }
    }

    public static class CatCounterReducer
            extends Reducer<Text,LongWritable,Text, LongWritable>
    {






        public void reduce(Text key,
                           Iterable<LongWritable> values,
                           Context context

        ) throws IOException, InterruptedException {

           //todo: reducer logic

        }
    }

    public static void main(String[] args) throws Exception {
        Configuration conf = new Configuration();

        //makes sure that all maps are completed before reducer starts to assure consistent counts
        //https://www.ibm.com/support/knowledgecenter/en/SSGSMK_7.1.1/mapreduce_user/map_reduce_configure_slowstart.html
        conf.set("mapreduce.job.reduce.slowstart.completedmaps","1.0");
        conf.set("mapreduce.task.timeout","180000");

        Job job = Job.getInstance(conf, "ComputeCategoryCounts");


        job.setJarByClass(Task1MapReduce.class);
        job.setMapperClass(CatCounterMapper.class);
        job.setCombinerClass(CatCounterReducer.class);
        job.setNumReduceTasks(1);
        job.setReducerClass(CatCounterReducer.class);
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(LongWritable.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(LongWritable.class);
        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));
        System.exit(job.waitForCompletion(true) ? 0 : 1);

    }

}
